
var e = document.getElementById("tabularStarsDiv");
e.style["display"] = "block";
e.style["margin"] = "1%";
e.style["width"] = "48%";
e.style["resize"] = "vertical";
// e.style["height"] = e.parentNode.clientHeight;
e.style["height"] = (window.innerHeight*0.5) + "px";
e.style["overflow"] = "auto";


var starTable = document.getElementById("tabularStarsBody");
var colNames = ["proper", "ra", "dec"];


console.log(stars[0][colNames[0]]);

//Insert data into the table from Json
for(i=0; i<stars.length; i++){
    var curRow = document.createElement("tr");
    // curRow.innerHTML = i;
    for(j=0; j<colNames.length; j++){
	var curCell = document.createElement("td");
	
	switch(j){
	case 1:
	    curCell.style = "text-align:right"
	    curCell.innerText = stars[i][colNames[j]].toFixed(2) + "h";
	    break;
	case 2:
	    curCell.style = "text-align:right"
	    curCell.innerText = stars[i][colNames[j]].toFixed(2) +
		decodeURIComponent("%C2%B0");
	    break;
	default:
	    curCell.style = "text-align:left"
	    curCell.innerText = stars[i][colNames[j]];
	}
	curRow.appendChild(curCell);
    }
    starTable.appendChild(curRow);
}

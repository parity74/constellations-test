//TAu > PI
Math.TAU = 2.0*Math.PI;

//Create canvas element
var viewerContainer = document.getElementById("viewerContainer");
var mainCanvas = document.createElement("canvas");
mainCanvas.id="viewer";
mainCanvas.style="border:1px solid #000000;width100%;height100%";
viewerContainer.appendChild(mainCanvas);

var coordsDisplay = document.createElement("div");
viewerContainer.appendChild(coordsDisplay);

var draw = mainCanvas.getContext("2d");

//Variables for mouse dragging
var mouse = {"x":0, "y":0};
var pMouse = {"x":0, "y":0};
var dMouse = {"x":0, "y":0};

var mouseEvent;
window.addEventListener('mousemove', function (e) {
    ra_offset = e.pageX/window.innerWidth*360.0;
    //Update only if mouse is in canvas
    if(0 < e.pageX && e.pageX < mainCanvas.width &&
       0 < e.pageY && e.pageY < mainCanvas.height)
    {
	mouse.x = e.pageX;
	mouse.y = e.pageY;
	//Mosue drag
	if(e.buttons ){
	    dMouse.x = mouse.x - pMouse.x;
	    dMouse.y = mouse.y - pMouse.y;
	}
    }
    else{
	dMouse.x = 0;
	dMouse.y = 0;
    }

})

window.addEventListener('touchmove', function (e) {
    ra_offset = e.pageX/window.innerWidth*360.0;
    //Update only if mouse is in canvas
    if(0 < e.touches[0].screenX && e.touches[0].screenX < mainCanvas.width &&
       0 < e.touches[0].screenY && e.touches[0].screenY < mainCanvas.height)
    {
	mouse.x = e.touches[0].screenX;
	mouse.y = e.touches[0].screenY;
	//Mosue drag
	if(e.buttons ){
	    dMouse.x = mouse.x - pMouse.x;
	    dMouse.y = mouse.y - pMouse.y;
	}
    }
    else{
	dMouse.x = 0;
	dMouse.y = 0;
    }
})

var ra_offset = 0;
var coord;

function dist(x1, y1, x2, y2){
    return(Math.sqrt(Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2)));
}

//Calculate projection
var theta = 0;
var phi = 0;
function projection(ra, dec){
    //Convert to radians
    ra = Math.TAU*(ra/24.0*360.0)/360.0;
    dec = Math.TAU*(dec/360.0);
    //Calcualte coordinates on sphere
    var x0 = Math.cos(dec) * Math.sin(ra);
    var y0 = Math.sin(dec);
    var z0 = Math.cos(dec) * Math.cos(ra);
    //Change pitch and yaw
    theta =  theta + (Math.TAU * -dMouse.x/mainCanvas.width/3);
    dPhi = (Math.TAU * dMouse.y/mainCanvas.height/3);
    //Constrain phi
    if(Math.TAU * -1/4 < phi + dPhi && phi + dPhi < Math.TAU * 1/4)
	phi = phi + dPhi;
    //Apply transformations
    x1 = x0*Math.cos(theta) + z0*Math.sin(theta);
    y1 = y0;
    z1 = z0*Math.cos(theta) - x0*Math.sin(theta);
    x2 = x1;
    y2 = y1*Math.cos(phi) - z1*Math.sin(phi);  
    z2 = z1*Math.cos(phi) + y1*Math.sin(phi);
    //Project sphere onto screen
    var x = -mainCanvas.height/2.50 * x2/(1+z2) + mainCanvas.width/2;
    var y = -mainCanvas.height/2.50 * y2/(1+z2) + mainCanvas.height/2;
    //Apply scaling
    x = 2*x - mainCanvas.width/2;
    y = 2*y - mainCanvas.height/2;
    //Return
    dMouse.x = 0;
    dMouse.y = 0;
    pMouse.x = mouse.x;
    pMouse.y = mouse.y;
    return {"x":x, "y":y}
}

//Calcualte raidus
var magF = 1;
function starRaidus(mag){
    return(magF * Math.abs(Math.min(mag, 6)-5));
}

//Redraw every 20 millisecs
setInterval( function(){
    //Show coordinates
    coordsDisplay.innerText =
	"RA: " + ((24-theta/Math.TAU*24)%24).toFixed(2) + "h" +
	", DEC: " + (phi/Math.TAU*360).toFixed(2) +
	decodeURIComponent("%C2%B0")

    //Set size
    var selected = stars[0];
    var minDist = Number.POSITIVE_INFINITY;
    // mainCanvas.width = window.innerHeight*.90;
    // mainCanvas.height = window.innerHeight*.90;
    mainCanvas.width = mainCanvas.parentNode.clientWidth*1;
    mainCanvas.height = mainCanvas.parentNode.clientWidth*1;
    
    //Background
    draw.clearRect(0, 0, mainCanvas.width, mainCanvas.height);
    draw.fillStyle = "black";
    draw.fillRect(0, 0, mainCanvas.width, mainCanvas.height);

    //Draw arcs
    
    var coords;
    for(var i=0; i<24; i++){
	for(var j=-90; j<90; j+=15){
	    //Draw latitude
	    draw.strokeStyle = "#FFF";
	    //Make equator red
	    if(j == 0)
		draw.strokeStyle = "#F00";
	    draw.beginPath();
	    coords = projection(i, j);
	    draw.moveTo(coords.x, coords.y);
	    coords = projection(i+1, j);
	    draw.lineTo(coords.x, coords.y);
	    draw.stroke();
	    draw.strokeStyle = "#FFF";
	    //Draw longitude
	    draw.beginPath();
	    coords = projection(i, j);
	    draw.moveTo(coords.x, coords.y);
	    coords = projection(i, j+15);
	    draw.lineTo(coords.x, coords.y);
	    draw.stroke();
	}
    }
    
    //Loopy
    for(i=0; i<stars.length; i++){
	//Calculate x, y, adn size
	coord = projection(stars[i].ra, stars[i].dec);
	var r = starRaidus(stars[i].mag);
	draw.fillStyle = "white";
	//Make Polaris red
	if(stars[i].proper == "Polaris")
	    draw.fillStyle = "red";
	//Make Sol transparent
	if(stars[i].proper == "Sol")
	    draw.fillStyle = "rgba(0,0,0,0)";
	//Draw the star
	draw.beginPath();
	draw.arc(coord.x, coord.y, r, 0, Math.TAU);
	draw.fill();
	//find star closest to cursor (not sol)
	var curDist = dist(mouse.x, mouse.y, coord.x, coord.y);
	if(minDist > curDist && stars[i].proper != "Sol"){
	    minDist = curDist;
	    selected = stars[i];
	}
    }
    mainCanvas.style.cursor = "crosshair";
    if(minDist < 15){
	mainCanvas.style.cursor = "none";

	//Get selection data
	var infoCoord = projection(selected.ra, selected.dec);
	var r = Math.max(starRaidus(selected.mag)*2, 5);
	//Highlight selection
	draw.strokeStyle = "red";
	draw.lineWidth = 2;
	draw.beginPath();
	draw.arc(infoCoord.x, infoCoord.y, r, 0, Math.TAU);
	draw.stroke();
	
	//Draw star info
	draw.fillStyle = "red";
	draw.font="12px Arial";
	r = r * 3;
	draw.fillText(selected.bf, infoCoord.x, infoCoord.y+r);
	draw.fillText(selected.proper, infoCoord.x, infoCoord.y+r+10);
    }
}, 20);
